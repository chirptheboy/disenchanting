The 1.18 version of this mod was built using the Cyclic's code and framework developed by Lothrazar.

Special thanks to
 - [Lothrazar](https://github.com/lothrazar/) for providing Cyclic's code under the MIT license
 - [McJty](https://github.com/McJtyMods) for his Forge modding tutorials
 - [Tsudico](https://github.com/Tsudico/Disenchanting) for the Disenchanting mod for Fabric
 - [akharding91](https://gitlab.com/akharding91) for the BDCraft textures