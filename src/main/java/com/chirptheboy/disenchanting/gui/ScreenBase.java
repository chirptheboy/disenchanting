package com.chirptheboy.disenchanting.gui;

import com.chirptheboy.disenchanting.ModDisenchanting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;

public abstract class ScreenBase<T extends AbstractContainerMenu> extends AbstractContainerScreen<T> {

  public ScreenBase(T screenContainer, Inventory inv, Component titleIn) {
    super(screenContainer, inv, titleIn);
  }

  protected void drawBackground(PoseStack ms, ResourceLocation gui) {
    RenderSystem.setShader(GameRenderer::getPositionTexShader);
    RenderSystem.setShaderTexture(0, gui);
    int relX = (this.width - this.imageWidth) / 2;
    int relY = (this.height - this.imageHeight) / 2;
    this.blit(ms, relX, relY, 0, 0, this.imageWidth, this.imageHeight);
  }

  protected void drawSlot(PoseStack ms, int x, int y, ResourceLocation texture) {
    drawSlot(ms, x, y, texture, 18);
  }

  protected void drawSlot(PoseStack ms, int x, int y, ResourceLocation texture, int size) {
    RenderSystem.setShader(GameRenderer::getPositionTexShader);
    RenderSystem.setShaderTexture(0, texture);
    blit(ms, leftPos + x, topPos + y, 0, 0, size, size, size, size);
  }


  /**
   * Translate the block name; and draw it in the top center
   *
   * @param name
   */
  protected void drawName(PoseStack ms, String name, int cost) {
    name = lang("block." + ModDisenchanting.MODID + "." + name);
    drawString(ms, name + ": " + cost,
        (this.getXSize() - this.font.width(name)) / 2,
        6.0F);
  }

  protected void drawCostString(PoseStack ms, String name, int cost, int color) {
    name = lang("gui." + ModDisenchanting.MODID + "." + name + ".cost");
    drawString(ms,
            name + ": ",
            7.0F,
            7.0F);
    drawString(ms,
            "" + cost,
            7.0F + this.font.width(name + ": "),
            7.0F); //color == 1 ? 4210752 : 0xbf634f);
  }

  protected void drawString(PoseStack ms, String name, float x, float y) {
    this.font.draw(ms, lang(name), x, y, 4210752);
  }

  protected void drawString(PoseStack ms, String name, float x, float y, int color) {
    this.font.draw(ms, lang(name), x, y, color);
  }

  public static String lang(String message) {
    TranslatableComponent t = new TranslatableComponent(message);
    return t.getString();
  }
}
