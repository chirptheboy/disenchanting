package com.chirptheboy.disenchanting.registry;

import com.chirptheboy.disenchanting.ModDisenchanting;
import com.chirptheboy.disenchanting.block.BlockDisenchanting;
import com.chirptheboy.disenchanting.block.disenchant.BlockDisenchant;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.ArrayList;
import java.util.List;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class BlockRegistry {

  public static List<BlockDisenchanting> BLOCKSCLIENTREGISTRY = new ArrayList<>();
  public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, ModDisenchanting.MODID);
  public static final RegistryObject<Block> DISENCHANTER = BLOCKS.register("disenchanter", () -> new BlockDisenchant(Block.Properties.of(Material.STONE)));

}
