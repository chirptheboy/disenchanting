package com.chirptheboy.disenchanting.registry;

import com.chirptheboy.disenchanting.ModDisenchanting;
import com.chirptheboy.disenchanting.block.disenchant.TileDisenchant;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class TileRegistry {

  public static final DeferredRegister<BlockEntityType<?>> TILES = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITIES, ModDisenchanting.MODID);
  public static final RegistryObject<BlockEntityType<TileDisenchant>> DISENCHANTER = TILES.register("disenchanter", () -> BlockEntityType.Builder.of(TileDisenchant::new, BlockRegistry.DISENCHANTER.get()).build(null));
}
