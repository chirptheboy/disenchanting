package com.chirptheboy.disenchanting.registry;

import com.chirptheboy.disenchanting.ModDisenchanting;
import com.chirptheboy.disenchanting.block.disenchant.ContainerDisenchant;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.common.extensions.IForgeMenuType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ContainerScreenRegistry {

  @SubscribeEvent
  public static void onContainerRegistry(final RegistryEvent.Register<MenuType<?>> event) {
    IForgeRegistry<MenuType<?>> r = event.getRegistry();
    r.register(IForgeMenuType.create((windowId, inv, data) -> {
      return new ContainerDisenchant(windowId, inv.player.level, data.readBlockPos(), inv, inv.player);
    }).setRegistryName("disenchanter"));
  }

  @ObjectHolder(ModDisenchanting.MODID + ":disenchanter")
  public static MenuType<ContainerDisenchant> DISENCHANTER;
}
