package com.chirptheboy.disenchanting;

import com.chirptheboy.disenchanting.config.ConfigRegistry;
import com.chirptheboy.disenchanting.data.DataTags;
import com.chirptheboy.disenchanting.registry.BlockRegistry;
import com.chirptheboy.disenchanting.registry.ClientRegistryDisenchanting;
import com.chirptheboy.disenchanting.registry.ItemRegistry;
import com.chirptheboy.disenchanting.registry.TileRegistry;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.ForgeMod;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;

@Mod(ModDisenchanting.MODID)
public class ModDisenchanting {

    /*
        Todo:
         - Fix ItemStackHandlerWrapper.extractItem to let you extract non-enchanted input items (like after they're disenchanted)
         - Fix quickMoveStack to let you move items between inventory and hotbar
         - Consider making ContainerDisenchant.disenchantItem server-side only (tried it, won't play the sound)
     */

    public static final String MODID = "disenchanting";
    public static final DisenchantingLogger LOGGER = new DisenchantingLogger(LogManager.getLogger());
    public static final ResourceLocation DISENCHANTER_GUI = new ResourceLocation(ModDisenchanting.MODID, "textures/gui/disenchanter.png");

    public ModDisenchanting() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientRegistryDisenchanting::setupClient);
        ConfigRegistry.setup();
        DataTags.setup();
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        BlockRegistry.BLOCKS.register(bus);
        ItemRegistry.ITEMS.register(bus);
        TileRegistry.TILES.register(bus);
        ForgeMod.enableMilkFluid();
    }
}
