package com.chirptheboy.disenchanting.block;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.network.protocol.game.ServerboundPlayerActionPacket;
import net.minecraft.world.Container;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import java.lang.ref.WeakReference;
import java.util.List;

public abstract class TileBlockEntityDisenchanting extends BlockEntity implements Container {

  public static final String NBTINV = "inv";
  public static final String NBTFLUID = "fluid";
  protected int flowing = 1;
  protected int needsRedstone = 1;
  protected int render = 0; // default to do not render
  protected int timer = 0;
  protected int cost = 0;
  protected int showCost = 1;
  protected int color = 0;


  public TileBlockEntityDisenchanting(BlockEntityType<?> tileEntityTypeIn, BlockPos pos, BlockState state) {
    super(tileEntityTypeIn, pos, state);
  }

  public Boolean showCost() {return showCost == 1;}
  public int getColor() {return color;}
  public int getCost() {return cost;}
  public int getTimer() {
    return timer;
  }

  protected Player getLookingPlayer(int maxRange, boolean mustCrouch) {
    List<Player> players = level.getEntitiesOfClass(Player.class, new AABB(
        this.worldPosition.getX() - maxRange, this.worldPosition.getY() - maxRange, this.worldPosition.getZ() - maxRange, this.worldPosition.getX() + maxRange, this.worldPosition.getY() + maxRange, this.worldPosition.getZ() + maxRange));
    for (Player player : players) {
      if (mustCrouch && !player.isCrouching()) {
        continue; //check the next one
      }
      //am i looking
      Vec3 positionEyes = player.getEyePosition(1F);
      Vec3 look = player.getViewVector(1F);
      //take the player eye position. draw a vector from the eyes, in the direction they are looking
      //of LENGTH equal to the range
      Vec3 visionWithLength = positionEyes.add(look.x * maxRange, look.y * maxRange, look.z * maxRange);
      //ray trayce from eyes, along the vision vec
      BlockHitResult rayTrace = this.level.clip(new ClipContext(positionEyes, visionWithLength, ClipContext.Block.OUTLINE, ClipContext.Fluid.NONE, player));
      if (this.worldPosition.equals(rayTrace.getBlockPos())) {
        //at least one is enough, stop looping
        return player;
      }
    }
    return null;
  }

 public static InteractionResult interactUseOnBlock(WeakReference<FakePlayer> fakePlayer,
      Level world, BlockPos targetPos, InteractionHand hand, Direction facing) throws Exception {
    if (fakePlayer == null) {
      return InteractionResult.FAIL;
    }
    Direction placementOn = (facing == null) ? fakePlayer.get().getMotionDirection() : facing;
    BlockHitResult blockraytraceresult = new BlockHitResult(
        fakePlayer.get().getLookAngle(), placementOn,
        targetPos, true);
    //processRightClick
    ItemStack itemInHand = fakePlayer.get().getItemInHand(hand);
    InteractionResult result = fakePlayer.get().gameMode.useItemOn(fakePlayer.get(), world, itemInHand, hand, blockraytraceresult);
    //it becomes CONSUME result 1 bucket. then later i guess it doesnt save, and then its water_bucket again
    return result;
  }

  /**
   * SOURCE https://github.com/Lothrazar/Cyclic/pull/1994 @metalshark
   */
  public static InteractionResult playerAttackBreakBlock(WeakReference<FakePlayer> fakePlayer,
      Level world, BlockPos targetPos, InteractionHand hand, Direction facing) throws Exception {
    if (fakePlayer == null) {
      return InteractionResult.FAIL;
    }
    try {
      fakePlayer.get().gameMode.handleBlockBreakAction(targetPos, ServerboundPlayerActionPacket.Action.START_DESTROY_BLOCK,
          facing, world.getMaxBuildHeight());
      return InteractionResult.SUCCESS;
    }
    catch (Exception e) {
      return InteractionResult.FAIL;
    }
  }

  public static boolean tryHarvestBlock(WeakReference<FakePlayer> fakePlayer, Level world, BlockPos targetPos) {
    if (fakePlayer == null) {
      return false;
    }
    return fakePlayer.get().gameMode.destroyBlock(targetPos);
  }

 public Direction getCurrentFacing() {
    if (this.getBlockState().hasProperty(BlockStateProperties.FACING)) {
      return this.getBlockState().getValue(BlockStateProperties.FACING);
    }
    if (this.getBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
      return this.getBlockState().getValue(BlockStateProperties.HORIZONTAL_FACING);
    }
    return null;
  }

  protected BlockPos getCurrentFacingPos(int distance) {
    Direction f = this.getCurrentFacing();
    if (f != null) {
      return this.worldPosition.relative(f, distance);
    }
    return this.worldPosition;
  }

  protected BlockPos getCurrentFacingPos() {
    return getCurrentFacingPos(1);
  }

  @Override
  public CompoundTag getUpdateTag() {
    CompoundTag syncData = super.getUpdateTag();
    this.saveAdditional(syncData);
    return syncData;
  }

  @Override
  public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt) {
    this.load(pkt.getTag());
    super.onDataPacket(net, pkt);
  }

  @Override
  public ClientboundBlockEntityDataPacket getUpdatePacket() {
    return ClientboundBlockEntityDataPacket.create(this);
  }

  public boolean isPowered() {
    return this.getLevel().hasNeighborSignal(this.getBlockPos());
  }

  public int getRedstonePower() {
    return this.getLevel().getBestNeighborSignal(this.getBlockPos());
  }

  public boolean requiresRedstone() {
    return this.needsRedstone == 1;
  }

  public boolean moveItems(Direction myFacingDir, int max, IItemHandler handlerHere) {
    return moveItems(myFacingDir, worldPosition.relative(myFacingDir), max, handlerHere, 0);
  }

  public boolean moveItems(Direction myFacingDir, BlockPos posTarget, int max, IItemHandler handlerHere, int theslot) {
    if (this.level.isClientSide()) {
      return false;
    }
    if (handlerHere == null) {
      return false;
    }
    Direction themFacingMe = myFacingDir.getOpposite();
    BlockEntity tileTarget = level.getBlockEntity(posTarget);
    if (tileTarget == null) {
      return false;
    }
    IItemHandler handlerOutput = tileTarget.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, themFacingMe).orElse(null);
    if (handlerOutput == null) {
      return false;
    }
    if (handlerHere != null && handlerOutput != null) {
      //first simulate
      ItemStack drain = handlerHere.extractItem(theslot, max, true); // handlerHere.getStackInSlot(theslot).copy();
      int sizeStarted = drain.getCount();
      if (!drain.isEmpty()) {
        //now push it into output, but find out what was ACTUALLY taken
        for (int slot = 0; slot < handlerOutput.getSlots(); slot++) {
          drain = handlerOutput.insertItem(slot, drain, false);
          if (drain.isEmpty()) {
            break; //done draining
          }
        }
      }
      int sizeAfter = sizeStarted - drain.getCount();
      if (sizeAfter > 0) {
        handlerHere.extractItem(theslot, sizeAfter, false);
      }
      return sizeAfter > 0;
    }
    return false;
  }

  @Override
  public void load(CompoundTag tag) {
    showCost = tag.getInt("showCost");
    color = tag.getInt("color");
    cost = tag.getInt("cost");
    flowing = tag.getInt("flowing");
    needsRedstone = tag.getInt("needsRedstone");
    render = tag.getInt("renderParticles");
    timer = tag.getInt("timer");
    super.load(tag);
  }

  @Override
  public void saveAdditional(CompoundTag tag) {
    tag.putInt("showCost", showCost);
    tag.putInt("color", color);
    tag.putInt("cost", cost);
    tag.putInt("flowing", flowing);
    tag.putInt("needsRedstone", needsRedstone);
    tag.putInt("renderParticles", render);
    tag.putInt("timer", timer);
    super.saveAdditional(tag);
  }

  public abstract void setField(int field, int value);

  public abstract int getField(int field);

  public void setNeedsRedstone(int value) {
    this.needsRedstone = value % 2;
  }

  public FluidStack getFluid() {
    return FluidStack.EMPTY;
  }

  public void setFluid(FluidStack fluid) {}

  /************************** IInventory needed for IRecipe **********************************/
  @Deprecated
  @Override
  public int getContainerSize() {
    return 0;
  }

  @Deprecated
  @Override
  public boolean isEmpty() {
    return true;
  }

  @Deprecated
  @Override
  public ItemStack getItem(int index) {
    return ItemStack.EMPTY;
  }

  @Deprecated
  @Override
  public ItemStack removeItem(int index, int count) {
    return ItemStack.EMPTY;
  }

  @Deprecated
  @Override
  public ItemStack removeItemNoUpdate(int index) {
    return ItemStack.EMPTY;
  }

  @Deprecated
  @Override
  public void setItem(int index, ItemStack stack) {}

  @Deprecated
  @Override
  public boolean stillValid(Player player) {
    return false;
  }

  @Deprecated
  @Override
  public void clearContent() {}

  public void setFieldString(int field, String value) {
    //for string field  
  }

  public String getFieldString(int field) {
    //for string field  
    return null;
  }

  /*public int getEnergy() {
    return this.getCapability(CapabilityEnergy.ENERGY).map(IEnergyStorage::getEnergyStored).orElse(0);
  }

  public void setEnergy(int value) {
    IEnergyStorage energ = this.getCapability(CapabilityEnergy.ENERGY).orElse(null);
    if (energ != null && energ instanceof CustomEnergyStorage) {
      ((CustomEnergyStorage) energ).setEnergy(value);
    }
  }*/

  //fluid tanks have 'onchanged', energy caps do not
  /*protected void syncEnergy() {
    if (level.isClientSide == false && level.getGameTime() % 20 == 0) { //if serverside then 
      IEnergyStorage energ = this.getCapability(CapabilityEnergy.ENERGY).orElse(null);
      if (energ != null) {
        PacketRegistry.sendToAllClients(this.getLevel(), new PacketEnergySync(this.getBlockPos(), energ.getEnergyStored()));
      }
    }
  }*/

  /*public void exportEnergyAllSides() {
    List<Integer> rawList = IntStream.rangeClosed(0, 5).boxed().collect(Collectors.toList());
    Collections.shuffle(rawList);
    for (Integer i : rawList) {
      Direction exportToSide = Direction.values()[i];
      moveEnergy(exportToSide, MENERGY / 2);
    }
  }*/
}
