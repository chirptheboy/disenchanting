package com.chirptheboy.disenchanting.config;

import com.chirptheboy.disenchanting.ModDisenchanting;
import com.chirptheboy.disenchanting.block.disenchant.TileDisenchant;
import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.loading.FMLPaths;

public class ConfigRegistry {

  private static final ForgeConfigSpec.Builder CFG = new ForgeConfigSpec.Builder();
    private static ForgeConfigSpec COMMON_CONFIG;

  static {
    initConfig();
  }

  private static void initConfig() {

    TileDisenchant.REQUIRES_EXPERIENCE = CFG.comment("Does disenchanting require experience?").define("requiresExperience", true);
    TileDisenchant.RANDOM_ENCHANTMENT = CFG.comment("Should the disenchanter select the a random enchantment on the enchanted item instead of the first one?").define("randomEnchantment", false);
    TileDisenchant.RESET_ANVIL_COST = CFG.comment("Should the enchanted item's anvil work cost be reset to zero when fully disenchanted?").define("resetAnvilCost", false);
    TileDisenchant.COST_SLIDER = CFG.comment("Sliding multiplier for the experience cost. Default is 2.").defineInRange("costSlider", 2,1,10);
    TileDisenchant.DAMAGE_ITEM = CFG.comment("Should the enchanted item be damaged during the disenchanting process?").define("damageItem", false);
    TileDisenchant.DAMAGE_PERCENT = CFG.comment("Percent of damage that should be done to the item on each disenchant.").defineInRange("damagePercent", 5,1,100);


    COMMON_CONFIG = CFG.build();
  }


  public static void setup() {
    final CommentedFileConfig configData = CommentedFileConfig.builder(FMLPaths.CONFIGDIR.get().resolve(ModDisenchanting.MODID + ".toml"))
        .sync()
        .autosave()
        .writingMode(WritingMode.REPLACE)
        .build();
    configData.load();
    COMMON_CONFIG.setConfig(configData);
  }
}
